import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
const x = document.querySelectorAll('h4')[1];
console.log(x.innerHTML);
const x2 = document.querySelector('.logo');
x2.innerHTML =
	'<img src="images/logo.svg" />' +
	'<span>Pizza<em>land</em></span>' +
	"<small>les pizzas c'est la vie</small>";
const monLien = document.querySelectorAll('body > footer a')[2];
console.log(monLien);
const x3 = document.querySelector('.pizzaListLink');
x3.setAttribute('class', 'active');
